package com.caioketo.gerenciadordeestudos.utils;

/**
 * Created by Caio on 17/09/2014.
 */
public class Util {
    public static String[] concat(String[]a,String[]b){
        if (a == null) return b;
        if (b == null) return a;
        String[] r = new String[a.length+b.length];
        System.arraycopy(a, 0, r, 0, a.length);
        System.arraycopy(b, 0, r, a.length, b.length);
        return r;
    }
}
