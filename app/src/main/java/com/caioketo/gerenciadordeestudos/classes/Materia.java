package com.caioketo.gerenciadordeestudos.classes;

/**
 * Created by Caio on 17/09/2014.
 */
public class Materia extends Base {
    private String descricao;

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String _descricao) {
        this.descricao = _descricao;
    }
}