package com.caioketo.gerenciadordeestudos.classes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Caio on 17/09/2014.
 */
public class Horario extends Base {
    private List<AulaHorario> aulas = new ArrayList<AulaHorario>();

    public List<Aula> getAulaByDia(int dia) {
        List<Aula> retorno = new ArrayList<Aula>();
        for (int i = 0; i < aulas.size(); i++) {
            if (aulas.get(i).getDia() == dia) {
                retorno.add(aulas.get(i).getAula());
            }
        }

        return retorno;
    }
}
