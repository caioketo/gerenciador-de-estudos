package com.caioketo.gerenciadordeestudos.classes;

/**
 * Created by Caio on 17/09/2014.
 */
public class Aula extends Base {
    private Materia materia;
    private int duracao;

    public int getDuracao() {
        return this.duracao;
    }

    public Materia getMateria() {
        return this.materia;
    }

    public void setDuracao(int _duracao) {
        this.duracao = _duracao;
    }

    public void setMateria(Materia _materia) {
        this.materia = _materia;
    }
}
