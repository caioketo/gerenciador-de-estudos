package com.caioketo.gerenciadordeestudos.classes;

/**
 * Created by Caio on 17/09/2014.
 */
public class Base {
    private int id;

    public int getId() {
        return this.id;
    }

    public void setId(int _id) {
        this.id = _id;
    }
}
