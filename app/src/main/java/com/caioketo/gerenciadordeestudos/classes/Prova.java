package com.caioketo.gerenciadordeestudos.classes;

import java.util.Date;

/**
 * Created by Caio on 17/09/2014.
 */
public class Prova extends Base {
    private Materia materia;
    private Date data;

    public Materia getMateria() {
        return this.materia;
    }

    public Date getData() {
        return this.data;
    }

    public void setMateria(Materia _materia) {
        this.materia = _materia;
    }

    public void setData(Date _data) {
        this.data = _data;
    }
}