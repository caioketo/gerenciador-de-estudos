package com.caioketo.gerenciadordeestudos.classes;

import java.sql.Time;

/**
 * Created by Caio on 17/09/2014.
 */
public class AulaHorario extends Base {
    private Aula aula;
    private int dia;
    private Time horario;


    public Aula getAula() {
        return this.aula;
    }

    public int getDia() {
        return this.dia;
    }

    public Time getHorario() {
        return this.horario;
    }

    public void setAula(Aula _aula) {
        this.aula = _aula;
    }

    public void setDia(int _dia) {
        this.dia = _dia;
    }

    public void setHorario(Time _horario) {
        this.horario = _horario;
    }
}
