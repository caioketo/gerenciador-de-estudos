package com.caioketo.gerenciadordeestudos.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Caio on 17/09/2014.
 */
public class TabelaAulaHorario implements TabelaBase{
    private static TabelaAulaHorario instance;

    public static TabelaAulaHorario getInstance() {
        if (instance == null) {
            instance = new TabelaAulaHorario();
        }
        return instance;
    }
    public static final String TABLE = "aula";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_AULA_ID = "aula_id";
    public static final String COLUMN_DIA = "dia";
    public static final String COLUMN_HORARIO = "horario";

    // SQL para criar a tabela
    private static final String DATABASE_CREATE = "create table "
            + TABLE
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_AULA_ID + " integer, "
            + COLUMN_DIA + " integer, "
            + COLUMN_HORARIO + " time "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }

    @Override
    public String getColumnID() {
        return COLUMN_ID;
    }

    @Override
    public String getTable() {
        return TABLE;
    }

    @Override
    public String[] getColumns() {
        String[] retorno = { COLUMN_AULA_ID, COLUMN_ID, COLUMN_DIA, COLUMN_HORARIO };
        return retorno;
    }
}
