package com.caioketo.gerenciadordeestudos.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Caio on 17/09/2014.
 */
public class TabelaProva implements TabelaBase {
    private static TabelaProva instance;

    public static TabelaProva getInstance() {
        if (instance == null) {
            instance = new TabelaProva();
        }
        return instance;
    }


    public static final String TABLE = "tabela";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_MATERIA_ID = "materia_id";
    public static final String COLUMN_DATA = "data";

    // SQL para criar a tabela
    private static final String DATABASE_CREATE = "create table "
            + TABLE
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_MATERIA_ID + " integer, "
            + COLUMN_DATA + " date "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }

    @Override
    public String getColumnID() {
        return COLUMN_ID;
    }

    @Override
    public String getTable() {
        return TABLE;
    }

    @Override
    public String[] getColumns() {
        String[] retorno = { COLUMN_DATA, COLUMN_MATERIA_ID, COLUMN_ID };
        return retorno;
    }
}
