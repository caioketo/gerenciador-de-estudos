package com.caioketo.gerenciadordeestudos.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Caio on 17/09/2014.
 */
public class TabelaAula implements TabelaBase{
    private static TabelaAula instance;

    public static TabelaAula getInstance() {
        if (instance == null) {
            instance = new TabelaAula();
        }
        return instance;
    }
    public static final String TABLE = "aula";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_MATERIA_ID = "materia_id";
    public static final String COLUMN_DURACAO = "duracao";

    // SQL para criar a tabela
    private static final String DATABASE_CREATE = "create table "
            + TABLE
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_MATERIA_ID + " integer, "
            + COLUMN_DURACAO + " integer "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }

    @Override
    public String getColumnID() {
        return COLUMN_ID;
    }

    @Override
    public String getTable() {
        return TABLE;
    }

    @Override
    public String[] getColumns() {
        String[] retorno = { COLUMN_DURACAO, COLUMN_ID, COLUMN_MATERIA_ID };
        return retorno;
    }
}
