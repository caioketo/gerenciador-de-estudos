package com.caioketo.gerenciadordeestudos.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.caioketo.gerenciadordeestudos.utils.Util;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by Caio on 17/09/2014.
 */
public class CustomContentProvider extends ContentProvider {
    private DatabaseHelper database;

    private static final String AUTHORITY = "com.caioketo.gerenciadordeestudos.database.customcontentprovider";

    // ------- Definimos URIs uma para cada tabela
    private static final String PATH_AULA = "aula";

    public static final Uri CONTENT_URI_AULA = Uri.parse("content://" + AUTHORITY
            + "/" + PATH_AULA);

    private static final String PATH_MATERIA = "materia";

    public static final Uri CONTENT_URI_MATERIA = Uri.parse("content://" + AUTHORITY
            + "/" + PATH_MATERIA);

    private static final String PATH_PROVA = "prova";

    public static final Uri CONTENT_URI_PROVA = Uri.parse("content://" + AUTHORITY
            + "/" + PATH_PROVA);

    private static final String PATH_AULA_HORARIO = "aula_horario";

    public static final Uri CONTENT_URI_AULA_HORARIO = Uri.parse("content://" + AUTHORITY
            + "/" + PATH_AULA_HORARIO);


    // ------- Configurando o UriMatcher
    private static final int AULA = 10;
    private static final int AULA_ID = 11;
    private static final int MATERIA = 12;
    private static final int MATERIA_ID = 13;
    private static final int PROVA = 14;
    private static final int PROVA_ID = 15;
    private static final int AULA_HORARIO = 16;
    private static final int AULA_HORARIO_ID = 17;
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, PATH_AULA, AULA);
        sURIMatcher.addURI(AUTHORITY, PATH_AULA + "/#", AULA_ID);

        sURIMatcher.addURI(AUTHORITY, PATH_MATERIA, MATERIA);
        sURIMatcher.addURI(AUTHORITY, PATH_MATERIA + "/#", MATERIA_ID);

        sURIMatcher.addURI(AUTHORITY, PATH_PROVA, PROVA);
        sURIMatcher.addURI(AUTHORITY, PATH_PROVA + "/#", PROVA_ID);

        sURIMatcher.addURI(AUTHORITY, PATH_AULA_HORARIO, AULA_HORARIO);
        sURIMatcher.addURI(AUTHORITY, PATH_AULA_HORARIO + "/#", AULA_HORARIO_ID);
    }

    @Override
    public boolean onCreate() {
        database = new DatabaseHelper(getContext());
        return false;
    }

    //@Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        // Vamos utilizar o queryBuilder em vez do método query()
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        TabelaBase tabela;
        boolean add_id = false;
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case MATERIA_ID:
                add_id = true;
            case MATERIA:
                tabela = TabelaMateria.getInstance();
                break;
            case AULA_ID:
                add_id = true;
            case AULA:
                tabela = TabelaAula.getInstance();
                break;
            case PROVA_ID:
                add_id = true;
            case PROVA:
                tabela = TabelaProva.getInstance();
                break;
            case AULA_HORARIO_ID:
                add_id = true;
            case AULA_HORARIO:
                tabela = TabelaAulaHorario.getInstance();
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        assert (tabela != null);
        if (add_id) {
            queryBuilder.appendWhere(tabela.getColumnID() + "="
                    + uri.getLastPathSegment());
        }
        queryBuilder.setTables(tabela.getTable());
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // Notificamos caso exista algum Listener
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    public String getPATH(int pathID) {
        switch (pathID) {
            case AULA:
                return PATH_AULA;
            case MATERIA:
                return PATH_MATERIA;
            case PROVA:
                return PATH_PROVA;
            case AULA_HORARIO:
                return PATH_AULA_HORARIO;
            default:
                return "";
        }
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        TabelaBase tabela;
        long id = 0;
        switch (uriType) {
            case AULA:
                tabela = TabelaAula.getInstance();
                break;
            case MATERIA:
                tabela = TabelaMateria.getInstance();
                break;
            case PROVA:
                tabela = TabelaProva.getInstance();
                break;
            case AULA_HORARIO:
                tabela = TabelaAulaHorario.getInstance();
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        id = sqlDB.insert(tabela.getTable(), null, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(getPATH(uriType) + "/" + id);
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        String id;
        TabelaBase tabela;
        boolean is_id = false;
        switch (uriType) {
            case AULA_ID:
                is_id = true;
            case AULA:
                tabela = TabelaAula.getInstance();
                break;
            case MATERIA_ID:
                is_id = true;
            case MATERIA:
                tabela = TabelaMateria.getInstance();
                break;
            case PROVA_ID:
                is_id = true;
            case PROVA:
                tabela = TabelaProva.getInstance();
                break;
            case AULA_HORARIO_ID:
                is_id = true;
            case AULA_HORARIO:
                tabela = TabelaAulaHorario.getInstance();
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        if (!is_id) {
            rowsDeleted = sqlDB.delete(tabela.getTable(), selection,
                    selectionArgs);
        }
        else {
            id = uri.getLastPathSegment();
            if (TextUtils.isEmpty(selection)) {
                rowsDeleted = sqlDB.delete(tabela.getTable(),
                        tabela.getColumnID() + "=" + id,
                        null);
            } else {
                rowsDeleted = sqlDB.delete(tabela.getTable(),
                        tabela.getColumnID() + "=" + id
                                + " and " + selection,
                        selectionArgs);
            }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;
        String id;
        TabelaBase tabela;
        boolean is_id = false;
        switch (uriType) {
            case AULA_ID:
                is_id = true;
            case AULA:
                tabela = TabelaAula.getInstance();
                break;
            case MATERIA_ID:
                is_id = true;
            case MATERIA:
                tabela = TabelaMateria.getInstance();
                break;
            case PROVA_ID:
                is_id = true;
            case PROVA:
                tabela = TabelaProva.getInstance();
                break;
            case AULA_HORARIO_ID:
                is_id = true;
            case AULA_HORARIO:
                tabela = TabelaAulaHorario.getInstance();
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        if (!is_id) {
            rowsUpdated = sqlDB.update(tabela.getTable(),
                    values,
                    selection,
                    selectionArgs);
        }
        else {
            id = uri.getLastPathSegment();
            if (TextUtils.isEmpty(selection)) {
                rowsUpdated = sqlDB.update(tabela.getTable(),
                        values,
                        tabela.getColumnID() + "=" + id,
                        null);
            } else {
                rowsUpdated = sqlDB.update(tabela.getTable(),
                        values,
                        tabela.getColumnID() + "=" + id
                                + " and "
                                + selection,
                        selectionArgs);
            }
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = Util.concat(Util.concat(Util.concat(TabelaMateria.getInstance().getColumns(),
                        TabelaProva.getInstance().getColumns()), TabelaAula.getInstance().getColumns()),
                TabelaAulaHorario.getInstance().getColumns());
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }
}